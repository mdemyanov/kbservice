/**
 * Created by mdemyanov on 01.10.13.
 */
$(document).ready(function() {
    $('#kb_search').click(function() {
        $("#kbcontainer").empty();
        $.ajax({
            url: 'http://sd-gate.naumen.ru:10049/test',
            cache: false,
            timeout: 5000,
            data: {text: $('#search_string').val()},
            success: function(data) {
                var articles = $.parseJSON(data)
                console.log(articles.length);
                $.each(articles, function(index, article) {
                    console.log(article.title);
                    $("#kbcontainer").append($("<div/>", {
                        class: 'row article'
                    })
                        .append($("<div/>", {
                            class: 'small-13 large-13 columns',
                            html: '<p><strong>' + article.title + ':<strong> ' + article.body + '</p>'
                    })));
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('error ' + textStatus + " " + errorThrown);
            }
        });
    });
});
