/*
 * GET home page.
 */
var sys = require('util'),
    rest = require('../node_modules/restler'),
    _ = require('../node_modules/underscore'),
    lunr = require('../node_modules/lunr');

var index = lunr(function () {
    this.field('title', {boost: 10})
    this.field('body', {boost: 10})
    this.ref('id')
});

var articles;
rest.post('http://sd-gate.naumen.ru:10013/sd/services/rest/find/article?accessKey=698b4dd8-ddbd-467b-926c-57fe3dd8a692', {
    data: {FQN: 'article'}
}).on('complete', function (data) {
        if(data != null) {
            var i = 0;
            console.log(data.length);
            articles = data;
            _.each(articles, function(article) {
                console.log(article.title);
                console.log(article.text);
                console.log(i);
                index.add({
                    id: i++,
                    title: article.title,
                    body: article.text
                });
            });
            console.log("Проиндексированно " + i + " объектов");
            //console.log(index.search('ie'));
        }
    });

exports.index = function (req, res) {
    res.render('index', { title: 'Express' });
};
exports.finder = function (req, res) {
    res.render('finder', { title: 'Чем Вам помочь?' });
};
exports.test = function (req, res) {
    var resultId = index.search(req.query.text);
    var objects = [];
    _.each(resultId, function(id) {
        var article = articles[id.ref];
        console.log(articles[id.ref].title);
        objects.push({
            "title" : article.title,
            "body" : article.text
        });
    });
    res.writeHead(200, {'Con    tent-Type': 'text/plain'});
    console.log(objects);
    res.end(JSON.stringify(objects));
};